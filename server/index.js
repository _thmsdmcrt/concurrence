import { Thread } from '../src'
;(async () => {
  // const threads = Threads()

  // const task01 = async data => {
  //   const json = await fetch(data[0]).then(res => res.json())
  //   return json
  // }

  // const result = await threads.run(
  //   task01,
  //   ['https://reqres.in/api/users?page=2'],
  //   false
  // )

  // console.log(result)

  // for (let i = 0; i < 10; i++) {
  const thread = Thread(
    async data => {
      const response = await fetch(data[0]).then(res => res.json())
      return response
    },
    ['https://reqres.in/api/users?page=2']
  )
  // thread
  //   .run(['https://reqres.in/api/users?page=2'])
  //   .then(({ response, terminate }) => {})
})()
