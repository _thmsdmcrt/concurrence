# Concurrence

`concurrence` is a library design to run simple function in [Web Worker](https://developer.mozilla.org/en-US/docs/Web/API/Web_Workers_API/Using_web_workers) to promote multithreading in javascript.

## Documentation

- [Thread](https://gitlab.com/_thmsdmcrt/concurrence/wikis/Methods/Thread)
  - [Signature](https://gitlab.com/_thmsdmcrt/concurrence/wikis/Methods/Thread#signature)
  - [Exemples](https://gitlab.com/_thmsdmcrt/concurrence/wikis/Methods/Thread#exemples)
