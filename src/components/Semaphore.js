export function Semaphore (max) {
  const tasks = []

  let counter = max

  const dispatch = () => {
    if (counter > 0 && tasks.length > 0) {
      counter--
      tasks.shift()()
    }
  }

  const release = () => {
    counter++
    dispatch()
  }

  const acquire = () =>
    new Promise(resolve => {
      tasks.push(resolve)
      dispatch()
    })

  return async fn => {
    await acquire()
    let result
    try {
      result = await fn()
    } catch (e) {
      throw e
    } finally {
      release()
    }
    return result
  }
}

export let Limit = (max, fn) => {
  let semaphore = Semaphore(max)
  return (...args) => semaphore(() => fn(...args))
}
