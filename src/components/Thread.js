import { Worker } from './Worker'
import { Concurrency } from './Concurrency'
/**
 * @private
 * @description Execute Thread in accordance with hardwareConcurrency
 * @author Thomas Démocrite
 * @param {Object} concurrency - Execution limiter
 * @param {Function} task
 * @param {Array} tastArguments
 * @param {Boolean} autokill
 * @returns {Promise}
 */
function execute (concurrency, task, tastArguments, autokill) {
  return concurrency.run(async () => {
    const THREAD_RESPONSE = await Worker(task, autokill).run(tastArguments)
    return THREAD_RESPONSE
  })
}

/**
 * @private
 * @description Thread factory function
 */
const Thread = (function () {
  let concurrency = Concurrency()
  /**
   * @public
   * @description Run a task in a dedicated Web Worker.
   * @author Thomas Démocrite
   * @param {Function} task
   * @param {Array} [taskArguments=[]]
   * @param {Boolean} [autokill=true]
   * @param {Number} [hardwareConcurrency=concurrency.HARDWARE_CONCURRENCY]
   * @returns {Promise}
   */
  function thread (
    task,
    taskArguments = [],
    autokill = true,
    hardwareConcurrency = concurrency.HARDWARE_CONCURRENCY
  ) {
    if (hardwareConcurrency !== concurrency.HARDWARE_CONCURRENCY) {
      concurrency = Concurrency(hardwareConcurrency)
    }

    if (!taskArguments) {
      return {
        run: (params = []) => execute(concurrency, task, params, autokill)
      }
    }

    return execute(concurrency, task, taskArguments, autokill)
  }

  return thread
})()

export { Thread }
