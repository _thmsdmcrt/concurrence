/**
 * @private
 * @description Create a new Blob with type application/javascript.
 * @author Thomas Démocrite
 * @param {String} string
 * @returns {Blob}
 */
function blob (string) {
  return new window.Blob([string], { type: 'application/javascript' })
}
/**
 * @private
 * @description Create an url from a Blob.
 * @author Thomas Démocrite
 * @param {Blob} blob
 * @returns {URL}
 */
function url (blob) {
  return window.URL.createObjectURL(blob)
}
/**
 * @private
 * @description Populate Web Worker body with main thread provided function.
 * @author Thomas Démocrite
 * @param {Function} fn
 * @returns {String}
 */
function populate (fn) {
  const fnString = fn.toString()
  const worker = `
    const fn = ${fnString}
    self.onmessage = function (e) {
      Promise.resolve(e.data)
        .then(data => fn(data))
        .then(res => self.postMessage([e.data, "success", res]))
        .catch(err => self.postMessage([e.data, "error", err.toString()]))
    }
  `

  return worker
}
/**
 * @private
 * @description Worker factory function
 */
const Worker = (function () {
  const URLS_MAP = new Map()
  /**
   * @public
   * @description Create a dedicated Web Worker to run provided task.
   * @author Thomas Démocrite
   * @param {Function} task
   * @param {Boolean} [autokill=false]
   * @returns
   */
  function worker (task, autokill = false) {
    if (!('Worker' in window)) {
      throw new Error('Worker: Web Worker not supported.')
    }

    if (!task || typeof task !== 'function') {
      throw new TypeError(
        `Worker(task): @params "task": Expected {Function}, got ${typeof task}.`
      )
    }

    const FUNCTION_STRING = task.toString()

    if (!URLS_MAP.get(FUNCTION_STRING)) {
      URLS_MAP.set(FUNCTION_STRING, url(blob(populate(task))))
    }

    const URL = URLS_MAP.get(FUNCTION_STRING)
    const RESOLVER = []
    const WORKER = new window.Worker(URL)

    WORKER.onmessage = e => {
      const RESPONSE = { response: e.data }
      if (autokill) WORKER.terminate()
      else RESPONSE.terminate = WORKER.terminate
      RESOLVER.shift()[0](RESPONSE)
    }

    WORKER.onmessageerror = e => {
      const RESPONSE = { response: e.data }
      if (autokill) WORKER.terminate()
      else RESPONSE.terminate = WORKER.terminate
      RESOLVER.shift()[1](RESPONSE)
    }

    return {
      /**
       * @public
       * @description Run the dedicated task in the worker and pass it arguments.
       * @author Thomas Démocrite
       * @param {Array} args
       * @returns {Promise}
       */
      run (args = []) {
        if (!Array.isArray(args)) {
          throw new TypeError(
            `Worker#run(args): @params "args": Expected {Array}, got {${
              !args ? args : typeof args
            }}.`
          )
        }
        return new Promise((resolve, reject) => {
          WORKER.postMessage(args)
          RESOLVER.push([resolve, reject])
        })
      },
      /**
       * @public
       * @description Terminate the created Web Worker, clear the resolver and remove blob url from global Map.
       * @author Thomas Démocrite
       */
      terminate () {
        WORKER.terminate()
        RESOLVER.length = 0
        URLS_MAP.delete(task)
      }
    }
  }
  return worker
})()

export { Worker }
