import { Limit } from './Semaphore'

/**
 * @description Create a Semaphore for Thread execution.
 * @author Thomas Démocrite
 * @param {Numb} num
 * @returns {Object}
 */
export function Concurrency (num) {
  if (num && typeof num !== 'number') {
    throw new TypeError('@params "num": Expected {Number}.')
  }
  /** Core */
  const AVAILABLE_HARDWARE_CONCURRENCY = navigator.hardwareConcurrency || 4

  const HARDWARE_CONCURRENCY = !num
    ? AVAILABLE_HARDWARE_CONCURRENCY
    : Math.min(num, AVAILABLE_HARDWARE_CONCURRENCY)

  const RUN_TASK = fn => fn()
  /** Public Interface */
  return {
    HARDWARE_CONCURRENCY,
    run: Limit(HARDWARE_CONCURRENCY, RUN_TASK)
  }
}
